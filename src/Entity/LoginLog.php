<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LoginLogRepository")
 */
class LoginLog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $user_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $user_authority;

    /**
     * @ORM\Column(type="datetime")
     */
    private $logined_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $logouted_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getUserAuthority(): ?int
    {
        return $this->user_authority;
    }

    public function setUserAuthority(int $user_authority): self
    {
        $this->user_authority = $user_authority;

        return $this;
    }

    public function getLoginedAt(): ?\DateTimeInterface
    {
        return $this->logined_at;
    }

    public function setLoginedAt(\DateTimeInterface $logined_at): self
    {
        $this->logined_at = $logined_at;

        return $this;
    }

    public function getLogoutedAt(): ?\DateTimeInterface
    {
        return $this->logouted_at;
    }

    public function setLogoutedAt(\DateTimeInterface $logouted_at): self
    {
        $this->logouted_at = $logouted_at;

        return $this;
    }
}
