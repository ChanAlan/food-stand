<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\User;
use App\Form\UserType;

/**
 * User controller.
 * @Route("/api", name="api_users")
 */
class ApiUserController extends FOSRestController
{
	/**
	 * List all users
	 * @Rest\Get("/users")
	 *
	 * @return Response
	 */
	public function getAllUsersAction()
	{
		$repo = $this->getDoctrine()->getRepository(User::class);
		return $this->handleView($this->view($repo->findall()));
	}
}